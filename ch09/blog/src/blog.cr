require "json"
require "pg"
require "athena"
require "crinja"

require "./config"

require "./controllers/*"
require "./entities/*"
require "./services/*"

module Blog
  VERSION = "0.1.0"

  module Controllers; end
  module Entities; end

end

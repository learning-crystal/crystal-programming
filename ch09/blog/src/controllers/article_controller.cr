@[ADI::Register(public: true)]
class Blog::Controllers::ArticleController < ATH::Controller
  def initialize(@entity_manager : Blog::Services::EntityManager); end

  @[ARTA::Post("/article")]
  def create_article(
    @[ATHR::RequestBody::Extract]
    article : Blog::Entities::Article
  ) : Blog::Entities::Article
    @entity_manager.persist article
    article
  end

  @[ARTA::Put("/article/{id}")]
  def updated_article(
    id : Int64,
    @[ATHR::RequestBody::Extract]
    article : Blog::Entities::Article
  ) : Blog::Entities::Article
    article_entity = article_by_id id

    article_entity.title = article.title
    article_entity.body = article.body

    @entity_manager.persist article_entity
    article_entity
  end

  @[ARTA::Get("/article/{id}")]
  @[Blog::Annotations::Template("article.html.j2")]
  def article(id : Int64) : Blog::Entities::Article
    article_by_id id
  end

  @[ARTA::Get("/article")]
  def articles : Array(Blog::Entities::Article)
    @entity_manager.repository(Blog::Entities::Article).find_all
  end

  @[ARTA::Delete("/article/{id}")]
  def delete_article(id : Int64) : Nil
    @entity_manager.remove article_by_id(id)
  end

  private def article_by_id(id : Int64) : Blog::Entities::Article
    article = @entity_manager.repository(Blog::Entities::Article).find? id
    if article.nil?
      raise ATH::Exceptions::NotFound.new "An item with the provided ID could not be found."
    end
    article
  end
end

# blog

Sample blog application to experiment with external library _Athena_.

## Installation

To install required dependencies execute following command:
```shell
shards install
```

### Database setup

Postgres is run in Docker container defined in `docker-compose.yml` file.
To start container execute following command:
```shell
docker compose up -d 
```

#### Database migration

There are migration scripts located in `db` directory and which are copied to container's `/migrations` directory.
Those scripts should be run once to initialise database schema when run on host machine:
```shell
docker exec -it pg psql blog_user -d postgres -f /migrations/
000_setup.sql
docker exec -it pg psql blog_user -d postgres -f /migrations
/001_articles.sql
```

or this when run on container's terminal:
```shell
psql blog_user -d postgres -f /migrations/
000_setup.sql
psql blog_user -d postgres -f /migrations
/001_articles.sql
```

## Usage

To run application in development mode execute following command:
```shell
crystal src/server.cr
```

This will start _Athena_ server on default port `3000`.
To test is execute following command:
```shell
curl --request POST \
  --url http://localhost:3000/article \
  --header 'Content-Type: application/json' \
  --data '{
	"title": "My Title",
	"body": "My Body"
}'
```

It should produce following output:
```json
{
  "id": 1,
  "title": "My Title",
  "body": "My Body",
  "updated_at": "2023-04-23T20:44:34Z",
  "created_at": "2023-04-23T20:44:34Z"
}
```

## Development

TODO: Write development instructions here

## Contributors

- [Eduard Luhtonen](https://github.com/your-github-user) - creator and maintainer

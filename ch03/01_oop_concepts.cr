p 12.class             # => Int32
p "hello".class        # => String
p nil.class            # => Nil
p true.class           # => Bool
p [1, 2, "hey"].class  # => Array(Int32 | String)
puts

p "Crystal".size + 4    # => 11
# this is same as above
p("Crystal".size().+(4))    # => 11
puts

file = File.new("some_file.txt")
puts file.gets_to_end
file.close

# The previous example can be simplified by using a block
File.open("some_file.txt") do |file|
  puts file.gets_to_end
end

p 23.class          # => Int32
p Int32.class       # => Class
num = 10
type = Int32
p num.class == type # => true
p File.new("some_file.txt")        # => <File:some_file.txt>
file_class = File
p file_class.new("some_file.txt")  # => <File:some_file.txt>

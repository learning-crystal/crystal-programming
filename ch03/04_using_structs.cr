# Values and references – using structs
# =====================================
struct Address
    property state : String, city : String
    property line1 : String, line2 : String
    property zip : String
    def initialize(@state, @city, @line1, @line2, @zip)
    end
end

class Person
    property address : Address?
end

address = Address.new("CA", "Los Angeles", "Some fictitious line", "First house", "1234")
person1 = Person.new
person2 = Person.new
person1.address = address
address.zip = "ABCD"
person2.address = address
puts person1.address.try &.zip
puts person2.address.try &.zip
puts

struct Location
    property latitude = 0.0, longitude = 0.0
end
class Building
    property gps = Location.new
end
building = Building.new
building.gps.latitude = 1.5
p building

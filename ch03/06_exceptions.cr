# Exceptions
# ==========

def half(num : Int)
    if num.odd?
        raise "The number #{num} isn't even"
    end
    num // 2
end

begin
    p half(3)
rescue
    puts "can't compute half of 3!"
end
begin
    p half(3)
rescue error
    puts "can't compute half of 3 because of #{error}"
end

def half?(num)
    half(num)
rescue
    nil
end
p half? 2 # => 1
p half? 3 # => nil
p half? 4 # => 2

# rescue keyword can be used inline
def half?(num)
    half(num) rescue nil
end
p half? 6 # => 6
p half? 7 # => nil
p half? 8 # => 8

p half(4) # => 2
p half(5) # Unhandled exception: The number 5 isn't even (Exception)
p half(6) # This won't execute as we have aborted the program.

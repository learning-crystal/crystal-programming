# Custom exceptions

class OddNumberError < Exception
    def initialize(num : Int)
        super("The number #{num} isn't even")
    end
end
def half(num : Int32)
    if num.odd?
        raise OddNumberError.new(num)
    end
    num // 2
end

def half?(num)
    half(num)  
rescue error : OddNumberError
    nil
end

p half? 2 # => 1
p half? 3 # => nil
p half? 4 # => 2

p half(6) # => 3
p half(7) # Unhandled exception: The number 7 isn't even (Exception)
p half(8) # This won't execute as we have aborted the program.

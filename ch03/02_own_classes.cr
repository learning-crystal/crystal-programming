class SimplePerson
end

person1 = SimplePerson.new
person2 = SimplePerson.new

p person1        # You can display any object and inspect it
p person1.to_s   # Any object can be transformed into a String
p person1 == person2     # false. By default, compares by reference.
p person1.same?(person2) # Also false, same as above.
p person1.nil?           # false, person1 isn't nil.
p person1.is_a?(SimplePerson)  # true, person1 is an instance of SimplePerson.
puts

class Person
    def initialize(name : String)
        @name = name
        @age = 0
    end
    def age_up
        @age += 1
    end
    def name
        @name
    end
    def name=(new_name)
        @name = new_name
    end
end
jane = Person.new("Jane Doe")
p jane   # => #<Person:0x7f97ae6f3ea0 @name="Jane Doe", @age=0>
jane.name = "Mary"
5.times { jane.age_up }
p jane   # => #<Person:0x7f97ae6f3ea0 @name="Mary", @age=5>
puts

class Point
  def initialize(@x : Int32, @y : Int32)
  end
end
origin = Point.new(0, 0)
p origin
puts

class Cat
  @birthday = Time.local
  def adopt(name : String)
    @name = name
  end
end
my_cat = Cat.new
my_cat.adopt("Tom")
p my_cat

class Person1
  @name : String
  def initialize(first_name, last_name) 
    @name = first_name + " " + last_name
  end 
end

class Person2
  def initialize(first_name, last_name) 
    @name = "#{first_name} #{last_name}"
  end 
end

# Getters and setters
# add getter method
class Person
  def name
    @name 
  end 
end

# same with getter macro
class Person
  getter name : String
  getter age = 0
  getter height : Float64 = 1.78
end
puts
person = Person.new("John")
p person 

# add setter method
class Person
  def name=(new_name)
    @name = new_name
  end 
 end

# sane with setter method
class Person 
    setter name
end

person = Person.new("Tony")
p person
person.name = "Alfred"
p person

# property macro to add getter and setter methods to the class
class Person
    property age
end
person.age = 34
p person
puts

# Inheretance
# ===========
class Employee < Person
    property salary = 0
end
person = Person.new("Alan")
employee = Employee.new("Helen")
employee.salary = 10000
p person.is_a? Person    # => true
p employee.is_a? Person  # => true
p person.is_a? Employee  # => false
puts

class Employee
    def yearly_salary
        12 * salary
    end
end
class SalesEmployee < Employee
    property bonus = 0
    def yearly_salary
        super + @bonus
    end
end
employee = Employee.new("John")
p employee.yearly_salary
employee.salary = 1000
sales_employee = SalesEmployee.new("Jane")
p sales_employee.yearly_salary
sales_employee.salary = 1000
sales_employee.bonus = 3000
p employee.yearly_salary
p sales_employee.yearly_salary
puts

# Polymorphism
# ============
employee1 = Employee.new("Helen")
employee1.salary = 5000
employee2 = SalesEmployee.new("Susan")
employee2.salary = 4000
employee2.bonus = 20000
employee3 = Employee.new("Eric")
employee3.salary = 4000
employee_list = [employee1, employee2, employee3]

employee_list.each do |employee|
  puts "#{employee.name}'s yearly salary is $#{employee.yearly_salary.format(decimal_places: 2)}."
end
puts

# Abstract classes
# ================
abstract class Shape
end
class Circle < Shape
    def initialize(@radius : Float64)
    end
end
class Rectangle < Shape
    def initialize(@width : Float64, @height : Float64)
    end
end
a = Circle.new(4)
b = Rectangle.new(2, 3)
# next line won't compile because abstract class cannot be instantiated
# c = Shape.new # This will fail to compile; it doesn't make sense.

abstract class Shape
    abstract def area : Number
end
class Circle < Shape
    def area : Number
        Math::PI * @radius ** 2
    end
end
class Rectangle < Shape
    def area : Number
        @width * @height
    end
end

p a.area
p b.area
puts

# Class variables and class methods
# =================================
class Person
    @@next_id = 1
    @id: Int32
    def initialize(@name : String)
        @id = @@next_id
        @@next_id += 1
    end
end
first = Person.new("Adam")  # This will have @id = 1
second = Person.new("Jess") # And this will have @id = 2
p first 
p second
puts 

# class methods
class Person
    def self.reset_next_id
        @@next_id = 1
    end
end

# use of class_property macro to create class variable with class methods
class Person
    class_property next_id
end
p Person.next_id
Person.next_id = 3
p Person.next_id

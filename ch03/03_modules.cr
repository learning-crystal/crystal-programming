module WithSayName
    abstract def name : String
    def say_name
        puts "My name is #{name}"
    end
end

class Person
    include WithSayName
    property name : String
    def initialize(@name : String)
    end
end

def show(thing : WithSayName)
    thing.say_name
end
show Person.new("Jim")
puts

# Prints "Crystal Rocks!":
p Base64.decode_string("Q3J5c3RhbCBSb2NrcyE=")

text = "Crystal is cool!"
name = "John"
single_letter = 'X'
kana = '大' # International characters are always valid

p text
p name
p single_letter
p kana

name = "John"
age = 37
msg = "#{name} is #{age} years old" # Same as "John is 37 years old"
p msg

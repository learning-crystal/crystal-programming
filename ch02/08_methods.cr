def leap_year?(year)
  divides_by_4 = (year % 4 == 0)
  divides_by_100 = (year % 100 == 0)
  divides_by_400 = (year % 400 == 0)
  divides_by_4 && !(divides_by_100 && !divides_by_400)
end
puts leap_year? 1900 # => false
puts leap_year? 2000 # => true
puts leap_year? 2020 # => true
puts

# method overriding
def day_count(year)
  leap_year?(year) ? 366 : 365
end

def day_count(year, month)
  case month
  when 1, 3, 5, 7, 8, 10, 12
    31
  when 2
    leap_year?(year) ? 29 : 28
  else
    30
  end
end

puts day_count(2020)    # => 366
puts day_count(2021)    # => 365
puts day_count(2020, 2) # => 29
puts

def day_count(year, month)
  if month == 2
    return leap_year?(year) ? 29 : 28
  end
  month.in?(1, 3, 5, 7, 8, 10, 12) ? 31 : 30
end
puts day_count(2020, 2) # => 29
puts day_count(2023, 2) # => 28

def add(a, b) # 'a' and 'b' could be anything.
  a + b
end
p add(1, 2)            # Here they are Int32, prints 3.
p add("Crys", "tal")   # Here they are String, prints "Crystal".
# Let's try to cause issues: 'a' is Int32 and 'b' is String.
#p add(3, "hi")
# => Error: no overload matches 'Int32#+' with type String
puts

# Adding type restrictions
# ========================
def show(value : String)
  puts "The string is '#{value}'"
end
def show(value : Int)
  puts "The integer is #{value}"
end
show(12)      # => The integer is 12
show("hey")   # => The string is 'hey'
# next line won't compile
#show(3.14159) # Error: no overload matches 'show' with type Float64
x = rand(1..2) == 1 ? "hey" : 12
show(x)  # => Either "The integer is 12" or "The string is 'hey'"

def show_type(value : Int | String)
  puts "Compile-time type is #{typeof(value)}."
  puts "Runtime type is #{value.class}."
  puts "Value is #{value}."
end
show_type(10)
# => Compile-time type is Int32.
# => Runtime type is Int32.
# => Value is 10.
x = rand(1..2) == 1 ? "hello" : 5_u8
show_type(x)
# => Compile-time type is (String | UInt8).
# => Runtime type is String.
# => Value is hello.

# return type restriction
def add2(a, b) : Int
  a + b
end
add2 1, 3     # => 4
# next line won't compile
#add "a", "b" # Error: method top-level add must return Int but it is returning String
puts

# Default values
# ==============
def random_score(base, max = 10)
  base + rand(0..max)
end
p random_score(5)    # => Some random number between 5 and 15.
p random_score(5, 5) # => Some random number between 5 and 10.
puts

# Named parameters
# ================
def store_opening_time(is_weekend, is_holiday)
  if is_holiday
    is_weekend ? nil : "8:00"
  else
    is_weekend ? "12:00" : "9:00"
  end
end

p store_opening_time(true, false) # What is 'true' and 'false' here?
p store_opening_time(is_weekend: true, is_holiday: false) # this is much clearer

# enfore use of named parameters
def store_opening_time2(*, is_weekend, is_holiday)
  store_opening_time(is_weekend, is_holiday)
end
p store_opening_time2(is_weekend: true, is_holiday: false)
p store_opening_time2(is_weekend: true, is_holiday: false)
# next line fails to compile: Error: missing arguments: is_weekend, is_holiday
#p store_opening_time2(true, false) # Invalid!
puts

# External and internal names for parameters
# ==========================================
def multiply(value, *, by factor, adding term = 0)
  value * factor + term
end
p multiply(3, by: 5)             # => 15
p multiply(2, by: 3, adding: 10) # => 16
puts

# Passing blocks to methods
# =========================
def perform_operation
  puts "before yield"
  yield
  puts "between yields"
  yield
  puts "after both yields"
end

# to pass a block can use curly braces {} or do..end
perform_operation {
  puts "inside block"
}
puts "===="
perform_operation do
  puts "inside block"
end
puts

def transform(list)
  i = 0
  # new_list is an Array made of whatever type the block
  # returns
  new_list = [] of typeof(yield list[0])
  while i < list.size
    new_list << yield list[i]
    i += 1
  end
  new_list
end
numbers = [1, 2, 3, 4, 5]
p transform(numbers) { |n| n ** 2 } # => [1, 4, 9, 16, 25]
p transform(numbers) { |n| n.to_s } # => ["1", "2", "3", "4", "5"]

# array have map method
numbers = [1, 2, 3, 4, 5]
p numbers.map { |n| n ** 2 }   # => [1, 4, 9, 16, 25]
p numbers.map { |n| n.to_s }   # => ["1", "2", "3", "4", "5"]
puts

# Using next inside a block
def generate
  first = yield 1
  second = yield 2
  third = yield 3
  first + second + third
end
result = generate do |x|
  if x == 2
    next 10
  end
  x + 1
end
p result

# Using break inside a block
result = generate do |x|
  if x == 2
    break 10 # break instead of next
  end
  x + 1
end
p result
puts

# Returning from inside a block
def collatz_sequence(n)
  while true
    n = if n.even?
      n // 2
    else
      3 * n + 1
    end
    yield n
  end
end
def sequence_length(initial)
  length = 0
  collatz_sequence(initial) do |x|
    puts "Element: #{x}"
    length += 1
    if x == 1
      return length     # <= Note this 'return'
    end
  end
end
puts "Length starting from 14 is: #{sequence_length(14)}"

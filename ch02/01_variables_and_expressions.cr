score = 38
distance = 104
scoure = 41
p score

# Assign two variables at once
emma, josh = 19, 16
# This is the same, in two lines
emma = 19
josh = 16
# Now swap their values
emma, josh = josh, emma
p emma # => 16
p josh # => 19

FEET   = 0.3048 # Meters
INCHES = 0.0254 # Meters
my_height = 6 * FEET + 2 * INCHES   # 1.87960 meters
#FEET = 20 # Error: already initialized constant FEET

small_number = 47            # This is of type Int32
larger_number = 8795656243   # Now this is of type Int64
very_compact_number = 47u8   # Type is UInt8 because of the
# suffix
other_number = 1_234_000     # This is the same as 1234000
negative_number = -17        # There are also negative
# values
# next line doesn't compile
#invalid_number = 547_u8      # 547 doesn't fit UInt8's
# range
pi = 3.141592653589          # Fractional numbers are
# Float64
imprecise_pi = 3.14159_f32   # This is a Float32

hero_health_points = 100
hero_defense = 7
enemy_attack = 16
damage = enemy_attack - hero_defense # The enemy causes 9
# damage
hero_health_points -= damage # Now the hero health points
# is 91
healing_factor = 0.05 # The hero heals at a rate of 5% per
# turn
recovered_health = hero_health_points * healing_factor
hero_health_points += recovered_health # Now the health is
# 95.55
p "hero health points: #{hero_health_points}"

# This same calculation can also be done in a single line:
result = (100 - (16 - 7)) * (1 + 0.05) # => 95.55
p result

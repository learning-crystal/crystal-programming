enum UserKind
  Guest
  Regular
  Admin
end

# Enum
user_kind = UserKind::Regular
puts "This user is of kind #{user_kind}"

# Symbol
user_kind = :regular
puts "This user is of kind #{user_kind}"

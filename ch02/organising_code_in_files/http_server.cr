require "http/server"  # Imports the HTTP server from stdlib.
server = HTTP::Server.new do |context|
  context.response.content_type = "text/plain"
  context.response.print "Hello world, got #{context.request.path}!"
end
port = 8080
puts "Listen on http://127.0.0.1:#{port}"
server.listen(port)

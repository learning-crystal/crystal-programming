p 1..5         # => 1, 2, 3, 4, and 5.
p 1...5        # => 1, 2, 3, and 4.
p 1.0...4.0    # => Includes 3.9 and 3.999999, but not 4.'
p 'a'..'z'     # => All the letters of the alphabet
p "aa".."zz"   # => All combinations of two letters

1..      # => All numbers greater than 1
...0     # => Negative numbers, not including zero
..       # => A range that includes everything, even itself

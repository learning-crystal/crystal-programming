# Arrays and tuples
# =================
# Array is initialised with square brackets []
numbers = [1, 2, 3, 4]     # This is of type Array(Int32)
numbers << 10
puts "The #{numbers.size} numbers are #{numbers}" # => The 5 numbers are [1, 2, 3, 4, 10]

# next line will not compile
# numbers << "oops" # Error: no overload matches 'Array(Int32)#<<' with type String
puts

# use of union types in arrays
first_list = [1, 2, 3, "abc", 40]
p typeof(first_list)   # => Array(Int32 | String)
first_list << "hey!"   # Ok
# Now all elements are unions:
element = first_list[0]
p element         # => 1
p element.class   # => Int32
p typeof(element) # => Int32 | String
# Types can also be explicit:
second_list = [1, 2, 3, 4] of Int32 | String
p typeof(second_list)   # => Array(Int32 | String)
second_list << "hey!"   # Ok
# When declaring an empty array, an explicit type is mandatory:
empty_list = [] of Int32
puts

# Tuples
# tuples initialised with curly braces {}
list = {1, 2, "abc", 40}
p typeof(list)    # => Tuple(Int32, Int32, String, Int32)
element = list[0]
p typeof(element) # => Int32
# next line won't compile
#list << 10   # Invalid, tuples are immutable.
puts

# Hash
# ====
population = {
"China"         => 1_439_323_776,
"India"         => 1_380_004_385,
"United States" => 331_002_651,
"Indonesia"     => 273_523_615,
"Pakistan"      => 220_892_340,
"Brazil"        => 212_559_417,
"Nigeria"       => 206_139_589,
"Bangladesh"    => 164_689_383,
"Russia"        => 145_934_462,
"Mexico"        => 128_932_753,
}
# next line will fail with Overflow error, because hash is initialised as Hash(String, Int32)
#puts "Total population: #{population.values.sum}"

# to solve problem above should declare explicit type for this hash
population = {
"China"         => 1_439_323_776,
"India"         => 1_380_004_385,
"United States" => 331_002_651,
"Indonesia"     => 273_523_615,
"Pakistan"      => 220_892_340,
"Brazil"        => 212_559_417,
"Nigeria"       => 206_139_589,
"Bangladesh"    => 164_689_383,
"Russia"        => 145_934_462,
"Mexico"        => 128_932_753,
} of String => Int64
puts "Total population: #{population.values.sum}"
puts

# Iterating collections with blocks
5.times do
  puts "Hello!"
end
(10..15).each do |x|
  puts "My number is #{x}"
end
["apple", "orange", "banana"].each do |fruit|
  puts "Don't forget to buy some #{fruit}s!"
end
puts

# Short block syntax
fruits = ["apple", "orange", "banana"]
# (1) Prints ["APPLE", "ORANGE", "BANANA"]
p(fruits.map do |fruit|
  fruit.upcase
end)
# (2) Same result, braces syntax
p fruits.map { |fruit| fruit.upcase }
# (3) Same result, short block syntax
p fruits.map &.upcase
puts

# Splat parameters
def get_pop(population, *countries)
  puts "Requested countries: #{countries}"
  countries.map { |country| population[country] }
end
puts get_pop(population, "Indonesia", "China", "United States")

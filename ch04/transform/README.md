# transform

TODO: Write a description here

## Installation

1. Add the dependency to your `shard.yml`:

   ```yaml
   dependencies:
     transform:
       github: your-github-user/transform
   ```

2. Run `shards install`

## Usage

```crystal
require "transform"
```

TODO: Write usage instructions here

## Run in development mode

To run this application using `crystal` compiler execute following command:
```shell
crystal src/transform_cli.cr '[.[] | {"id": (.id + 1), "name": .author.name}]'
```

or with `run`:
```shell
crystal run src/transform_cli.cr -- '[.[] | {"id": (.id + 1), "name": .author.name}]'
```

## Contributors

- [Eduard Luhtonen](https://github.com/your-github-user) - creator and maintainer

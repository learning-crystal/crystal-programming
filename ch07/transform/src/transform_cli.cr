require "./transform"
require "option_parser"

processor = Transform::Processor.new
multi_file_mode = false

OptionParser.parse do |parser|
  parser.banner = "Usage: tranform <filter> [options] [arguments] [filname...]"
  parser.on("-m", "--multi", "Enables multiple file intput mode") { multi_file_mode = true }
  parser.on("-h", "--help", "Show this help") do
    puts parser
    exit
  end
end

begin
  if multi_file_mode
    processor.process_multiple ARGV.shift, ARGV, STDERR
  else
    processor.process ARGV, STDIN, STDOUT, STDERR
  end
rescue ex : RuntimeError | Exception
  puts "Failed: #{ex}"
  exit 1
end

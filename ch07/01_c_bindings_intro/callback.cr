@[Link(ldflags: "#{__DIR__}/callback.o")]
lib LibCallBack
  fun number_callback(callback : LibC::Int -> Void) : Void
end

LibCallBack.number_callback ->(value) { puts "Generated: #{value}" }

# Chapter 7: C Interoperability

This chapter is going to focus on one of the more advanced Crystal features: the ability to interop with existing C libraries by writing __C Bindings__.

This project includes several C source files which should be compiled first to be used in Crystal program.

It can be done with the following command (example is `hello.c`):
```shell
gcc -Wall -O3 -march=native -c hello.c -o hello.o
```
Or using `Makefile` included in this project with the following command:
```shell
make
```

Both commands will compile C source file `hello.c` into object file `hello.o` with native support.
This compiled object file can be used in the Crystal program. Crystal program can be compile and run with this simple command:
```shell
crystal hello.cr
```
Specific C file can be compile with the following command: 
```shell
make struct
```

This will compile `struct.c` file and product `struct.o` file.

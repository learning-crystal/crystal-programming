require "./transform"

begin
  Transform::Processor.new.process ARGV, STDIN, STDOUT, STDERR
rescue ex : RuntimeError
  exit 1
end

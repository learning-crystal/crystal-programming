# transform

TODO: Write a description here

## Installation

1. Add the dependency to your `shard.yml`:

   ```yaml
   dependencies:
     transform:
       github: your-github-user/transform
   ```

2. Run `shards install`

## Usage

```crystal
require "transform"
```

TODO: Write usage instructions here

## Run in development mode

To run this application using `crystal` compiler execute following command:
```shell
echo $'---\n- id: 1\n  author:\n    name: Jim\n- id: 2\n  author:\n    name: Bob\n' | crystal src/transform_cli.cr '[.[] | {"id": (.id + 1), "name": .author.name}]'
```

or with `run`:
```shell
echo $'---\n- id: 1\n  author:\n    name: Jim\n- id: 2\n  author:\n    name: Bob\n' | crystal run src/transform_cli.cr -- '[.[] | {"id": (.id + 1), "name": .author.name}]'
```

## Performance test

There are 2 implementation versions of `serialize` and `deserialize` methods in `yaml.cr` file.
First loads entire data into memory and do transformation in memory. 
Second version transforms data in the loop loading only a part of the data, which will be much more performant when 
dealing with larger data.

To test performance of both implementations comment one version and uncomment the other. 
Build the project with the following command:
```shell
shards build --release
```

In this test larger example file can be used. It can be downloaded from this location:
https://github.com/PacktPublishing/Crystal-Programming/blob/main/Chapter05/invItems.yaml

To run benchmark execute following command:
```shell
/usr/bin/time ./bin/transform . invItems.yaml > /dev/null
```

Look for the line with the following text `Maximum resident set size (kbytes)`, this will contain information about memory used by the process.

After that swap the implementation versions, rebuild project and rerun benchmark.
Compare results.

## Contributors

- [Eduard Luhtonen](https://github.com/your-github-user) - creator and maintainer

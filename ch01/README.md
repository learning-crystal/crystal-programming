# Chapter 1: An Introduction to Crystal

## Traditional "Hello World"

This is traditional "Hello World" application in Crystal.

To run application execute following command:
```shell
crystal run hello.cr
```

## More advanced hello application

This is more advanced example of "hello" application in Crystal. It will as user for it's name and then will print hello with the given name.

To run it execute following command:
```shell
crystal run hello_name.cr
```

## Create an executable

It is possible to create an executable file from the Crystal source script. To do that, execute following command:
```shell
crystal build --release hello.cr
```

This will create a binary file `hello` in the current directory, which can be run with this simple command:
```shell
./hello
```
